﻿using UnityEngine;
using System.Collections;

public class DestroyBullet : MonoBehaviour {

    public GameObject explosion;


    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            DestroyObject(other.gameObject);
        }
        else if (other.gameObject.tag == "Rocket")
        {
            DestroyObject(other.gameObject);
            Instantiate(explosion, other.transform.position, other.transform.rotation);
        }

    }
}
