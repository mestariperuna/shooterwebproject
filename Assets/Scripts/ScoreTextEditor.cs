﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class ScoreTextEditor : MonoBehaviour {

    public List<Text> textBlocks;
    public string nameToSearch;
    InputField _inputField;
    public int mapIndex;
    public string searchString;

    private void Start()
    {
        // Delegates.
        HttpManager.instance.OnPlayerReceived += OnPlayerReceived;
        HttpManager.instance.OnPlayersReceived += OnPlayersReceived;
        HttpManager.instance.OnAverageMapDataReceived += OnAverageMapDataReceived;

        _inputField = this.GetComponentInChildren<InputField>();
    }

    // Inputfieldin nimen settaus ja haku
    public void SetPlayerName()
    {
        nameToSearch = _inputField.text;
    }

    public void SetMapIndex(int index)
    {
        mapIndex = index;
    }

    public void SetSearchString(string search)
    {
        searchString = search;
    }

    private void OnPlayerReceived(PlayerContainer player)
    {

        if (player != null)
        {
            for (int j = 0; j < textBlocks.Count; j++)
            {
                textBlocks[j].text = "";
            }

            textBlocks[1].text += "Map" + "\n";
            textBlocks[2].text += "Score" + "\n";
            textBlocks[3].text += "Kills" + "\n";
            textBlocks[4].text += "Lifetime" + "\n";
            textBlocks[5].text += "Fav. Weapon" + "\n";
            textBlocks[6].text += nameToSearch + "\n";

            // Haku pelaajan nimellä jokaiselle mapille.
            for(int i = 0; i<player.Maps.Count; i++)
            {
                textBlocks[1].text += player.Maps[i].MapName + "\n";
                textBlocks[2].text += player.Maps[i].Score + "\n";
                textBlocks[3].text += player.Maps[i].KillCount + "\n";
                textBlocks[4].text += string.Format("{0:0.00}", (player.Maps[i].LifeTime)) + "\n";
                textBlocks[5].text += player.Maps[i].FavouriteWeapon + "\n";
            }
            
        }
            
        else
            Debug.Log("player not found");
    }

    private void OnPlayersReceived(PlayerContainer[] players, int? map)
    {

        if (players != null)
        {
            // Nollataan tekstit
            for (int j = 0; j < textBlocks.Count; j++)
            {
                textBlocks[j].text = "";
            }

            textBlocks[0].text += "Rank" + "\n";
            textBlocks[1].text += "Name" + "\n";
            textBlocks[2].text += "Score" + "\n";
            textBlocks[3].text += "Kills" + "\n";
            textBlocks[4].text += "Lifetime" + "\n";
            textBlocks[5].text += "Fav. Weapon" + "\n";

            // Jos haetaan mapin perusteella
            if (map != null)
            {
                int mapindex = (int)map;
                textBlocks[6].text += players[0].Maps[mapindex].MapName + "\n";

                // Asetetaan teksteihin pelaajat
                for (int i = 0; i < players.Length; i++)
                {
                    textBlocks[0].text += i + 1 + "\n";
                    textBlocks[1].text += players[i].Name + "\n";
                    textBlocks[2].text += players[i].Maps[mapindex].Score + "\n";
                    textBlocks[3].text += players[i].Maps[mapindex].KillCount + "\n";
                    textBlocks[4].text += string.Format("{0:0.00}", (players[i].Maps[mapindex].LifeTime)) + "\n";
                    textBlocks[5].text += players[i].Maps[mapindex].FavouriteWeapon + "\n";
                }
            }
            // Jos oletushaku
            else
            {
                // Asetetaan teksteihin pelaajat
                for (int i = 0; i < players.Length; i++)
                {
                    textBlocks[0].text += i + 1 + "\n";
                    textBlocks[1].text += players[i].Name + "\n";
                    textBlocks[2].text += players[i].Maps[0].Score + "\n";
                    textBlocks[3].text += players[i].Maps[0].KillCount + "\n";
                    textBlocks[4].text += string.Format("{0:0.00}", (players[i].Maps[0].LifeTime)) + "\n";
                    textBlocks[5].text += players[i].Maps[0].FavouriteWeapon + "\n";
                }
            }
        }
        else
            Debug.Log("Something went wrong");
    }

    private void OnAverageMapDataReceived(double[] mapdata)
    {
        for (int j = 0; j < textBlocks.Count; j++)
        {
            textBlocks[j].text = "";
        }
        textBlocks[6].text += "Average scores\n";
        textBlocks[1].text += string.Format("Score\n{0:0.00}\n", (mapdata[0]));
        textBlocks[2].text += string.Format("Kills\n{0:0.00}\n", (mapdata[1]));
        textBlocks[3].text += string.Format("Lifetime\n{0:0.00\n}", (mapdata[2]));

    }

    public void DisplayAverageMapData()
    {
        HttpManager.instance.getAverageMapData();
    }

    public void DisplayPlayerByName()
    {
        HttpManager.instance.getPlayerByName(nameToSearch);
    }

    public void DisplayAllPlayers()
    {
        HttpManager.instance.GetPlayers();
    }

    public void DisplayPlayersByMap()
    {
        HttpManager.instance.GetTopScoreByMap(mapIndex, searchString);
    }
}
