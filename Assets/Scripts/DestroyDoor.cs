﻿using UnityEngine;
using System.Collections;



public class DestroyDoor : MonoBehaviour {

    public GameObject explosion;

    private int health = 600;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            health -= 15;
        }
        else if(other.gameObject.tag == "Rocket")
        {
            health -= 200;

        }
    }
    void Update()
    {
        if (health < 0)
        {
            Destroy(gameObject);
        }
    }
}


