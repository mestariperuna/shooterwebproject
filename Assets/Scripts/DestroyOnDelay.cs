﻿using UnityEngine;
using System.Collections;

public class DestroyOnDelay : MonoBehaviour {

	// Use this for initialization
	void Start () {

        Object.Destroy(gameObject, 1);

    }
	
	// Update is called once per frame
	void Update () {

        Object.Destroy(gameObject, 1);

    }

}
