﻿using UnityEngine;
using System.Collections;

public class Weapon{

    public string weaponname;
   
    protected int damage = 25;
    protected int ammoSpeed = 50;
    protected int currentAmmo = 30;
    protected int clipSize = 30;
    protected int reloadSpeed = 1;
    protected bool fullauto = false;
    protected float firerate = 1;
    protected int ammocarried = 0;
    protected int maxammocarried = 0;
    protected string name;
    public float timer;


    private WeaponManager weapons;


    //getterit ja setterit stateille

    public int getDamage()
    {
        return damage;
    }

    public int getAmmoSpeed()
    {
        return ammoSpeed;
    }

    public int getCurrentAmmo()
    {
        return currentAmmo;
    }

    public int getReloadSpeed()
    {
        return reloadSpeed;
    }

    public int getClipSize()
    {
        return clipSize;
    }


    public void setAmmo(int a)
    {
        currentAmmo += a;
    }

    public bool getFireMode()
    {
        return fullauto;
    }

    public void setFireRate(float r)
    {
        firerate = r;
    }

    public float getFireRate()
    {
        return firerate;
    }

    public int getAmmoCarried()
    {
        return ammocarried;
    }

    public void setAmmoCarried(int a)
    {
        ammocarried += a;
        if(ammocarried >= maxammocarried)
        {
            ammocarried = maxammocarried;
        }
    }

    public string getName()
    {
        return name;
    }


    //lataus

    public void Reload()
    {
        int tmp = 0;
        tmp = clipSize - currentAmmo;
        if (ammocarried <= tmp)
        {
            currentAmmo += ammocarried;
            ammocarried = 0;
        }
        else
        {
            currentAmmo += tmp;
            ammocarried -= tmp;
        }



    }


}


//eri aseet subclasseina
public class Rifle : Weapon
{
    public Rifle()
    {
        damage = 35;
        ammoSpeed = 50;
        currentAmmo = 30;
        clipSize = 30;
        reloadSpeed = 1;
        fullauto = true;
        firerate = 0.15f;
        ammocarried = 300;
        maxammocarried = 500;
        name = "Assault Rifle";

    }

}

public class RocketLauncher : Weapon
{
    public RocketLauncher()
    {
        damage = 50;
        ammoSpeed = 10;
        currentAmmo = 5;
        clipSize = 5;
        reloadSpeed = 1;
        fullauto = false;
        firerate = 1;
        maxammocarried = 10;
        ammocarried = 0;
        name = "Rocketlauncher";
    }

}

public class Shotgun : Weapon
{
    public Shotgun()
    {
        damage = 25;
        ammoSpeed = 20;
        currentAmmo = 8;
        clipSize = 8;
        reloadSpeed = 1;
        fullauto = false;
        firerate = 0.6f;
        ammocarried = 10;
        maxammocarried = 80;
        name = "Shotgun";
    }

}

public class Machinegun : Weapon
{
    public Machinegun()
    {
        damage = 50;
        ammoSpeed = 50;
        currentAmmo = 90;
        clipSize = 90;
        reloadSpeed = 3;
        fullauto = true;
        firerate = 0.10f;
        ammocarried = 0;
        maxammocarried = 270;
        name = "Machinegun";
    }
}
