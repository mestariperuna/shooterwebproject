﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Text;
using UnityEngine.SceneManagement;

public class ScoreWriter{

    public void SaveData(int score, string name)
    {

        // databasekamaa tulossa
        // sandbox.
        if(SceneManager.GetActiveScene().buildIndex == 1)
        {
            if(PlayerPrefs.GetInt("Highscore1") < score)
            {
                PlayerPrefs.SetInt("Highscore1", score);
            }            
        }
        // storage.
        else if (SceneManager.GetActiveScene().buildIndex == 2)
        {
            if (PlayerPrefs.GetInt("Highscore2") < score)
            {
                PlayerPrefs.SetInt("Highscore2", score);
            }              
        }
        // woods.
        else if (SceneManager.GetActiveScene().buildIndex == 3)
        {
            if (PlayerPrefs.GetInt("Highscore3") < score)
            {
                PlayerPrefs.SetInt("Highscore3", score);
            }
        }
    }
 
}


