﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class MapContainer{

    public string MapName { get; set; }
    public int Score { get; set; }
    public int KillCount { get; set; }
    public string FavouriteWeapon { get; set; }
    public float FavouriteWeaponTimer { get; set; }
    public float LifeTime { get; set; }

    public MapContainer(string name, int score, int killcount, string weapon, float weapontimer, float lifetime)
    {
        MapName = name;
        Score = score;
        KillCount = killcount;
        FavouriteWeapon = weapon;
        FavouriteWeaponTimer = weapontimer;
        LifeTime = lifetime;
    }
}
