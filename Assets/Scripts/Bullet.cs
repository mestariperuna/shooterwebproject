﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

    private Rigidbody2D ammo;
    private GameObject player;
    public int ammoSpeed;
    public GameObject bloodPrefab;

    private WeaponManager weapons;

    // Use this for initialization
    void Start () {

        player = GameObject.FindGameObjectWithTag("Player");
        weapons = player.GetComponent<WeaponManager>();

        ammo = GetComponent<Rigidbody2D>();
        Vector3 mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.rotation = Quaternion.LookRotation(Vector3.forward, mouse - transform.position);
        ammo.AddForce(transform.up * weapons.getWeapon().getAmmoSpeed(), ForceMode2D.Impulse);
    }
	
	// Update is called once per frame
	void Update () {
       

    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Enemy")
        {
            GameObject bloodspill =  Instantiate(bloodPrefab, transform.position, transform.rotation) as GameObject;
            Destroy(bloodspill, 3f);
        }

        if (col.gameObject.tag == "Obstacle")
        {
            Destroy(gameObject);
        }
    }


            void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
