﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    GameObject[] pauseObjects;
    public GameObject crosshair;
    public Button continueButton;

    // Use this for initialization
    void Start () {

        Time.timeScale = 1;
        pauseObjects = GameObject.FindGameObjectsWithTag("ShowOnPause");
        //crosshair = GameObject.FindGameObjectWithTag("Crosshair");
        hidePaused();

        //continueButton = GameObject.FindGameObjectWithTag("ContinueHTTP").GetComponent<Button>();
        continueButton.onClick.AddListener(SendData);
	
	}

    void SendData()
    {
        HttpManager.instance.postPlayerData();
    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (Time.timeScale == 1)
            {
                Time.timeScale = 0;
                showPaused();
                
            }
            else if (Time.timeScale == 0)
            {
                Time.timeScale = 1;
                hidePaused();
                
            }
        }
	
	}

    public void PauseControl()
    {
        if(Time.timeScale == 1)
        {
            Time.timeScale = 0;
            showPaused();
        }
        else if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
            hidePaused();
        }
    }

    public void Reload()
    {
        SceneManager.LoadScene(1);
    }

    public void LoadScene()
    {
        SceneManager.LoadScene(0);
    }


    public void hidePaused()
    {
        foreach(GameObject g in pauseObjects)
        {
            g.SetActive(false);
        }
        crosshair.SetActive(true);
        Cursor.visible = false;
    }

    public void showPaused()
    {
        foreach(GameObject g in pauseObjects)
        {
            g.SetActive(true);
        }
        crosshair.SetActive(false);
        Cursor.visible = true;
    }

    public void Quit()
    {
        Application.Quit();
    }

}
