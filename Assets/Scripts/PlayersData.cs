﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayersData
{
    public PlayerContainer[] PlayersDataArray { get; set; }
}
