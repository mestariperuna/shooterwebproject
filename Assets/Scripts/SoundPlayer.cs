﻿using UnityEngine;
using System.Collections;

public class SoundPlayer : MonoBehaviour {

    public AudioSource deathsound;
	// Use this for initialization
	void Start () {

        deathsound.Play();
        Destroy(gameObject, 2f);

	}
}
