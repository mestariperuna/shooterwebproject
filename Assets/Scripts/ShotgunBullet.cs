﻿using UnityEngine;
using System.Collections;

public class ShotgunBullet : MonoBehaviour
{

    private Rigidbody2D ammo;
    private GameObject player;
    public int ammoSpeed;
    public GameObject bloodPrefab;

    private WeaponManager weapons;



    // Use this for initialization
    void Start()
    {

        player = GameObject.FindGameObjectWithTag("Player");
        weapons = player.GetComponent<WeaponManager>();

        ammo = GetComponent<Rigidbody2D>();
        ammo.AddForce(transform.up * weapons.getWeapon().getAmmoSpeed(), ForceMode2D.Impulse);


    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Enemy")
        {
            Instantiate(bloodPrefab, transform.position, transform.rotation);
            //Object.Destroy(bloodPrefab, 10);
        }
    }

    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
