﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Enemy : MonoBehaviour {


    private Transform target;
    public float speed = 10;
    public float health = 100;
    private bool move = true;
    private GameObject player;
    private PlayerController targetplayer;
    private WeaponManager weapons;
    public int scoreworth;
    public int damage;


    public float attackTime;
    public float coolDown;

    public GameObject bloodimage;
    public GameObject deathsound;

    public GameObject spawnerprefab;
    private GameObject droppedweapon;
    public GameObject hppack;
    public GameObject rifleammo;
    public GameObject shotgunammo;
    public GameObject rocketammo;
    public GameObject machinegunammo;

    private int droprate;



	// Use this for initialization
	void Start () {

        player = GameObject.FindGameObjectWithTag("Player");
        target = player.transform;
        targetplayer = player.GetComponent<PlayerController>();

        weapons = player.GetComponent<WeaponManager>();

        attackTime = 0;
        coolDown = 2.0f;


    }
	
	// Update is called once per frame
	void Update () {

     /*   if(Time.time >= changedir)
        {
            float randomX = Random.Range(-2.0f, 2.0f);
            float randomY = Random.Range(-2.0f, 2.0f);
            changedir = Time.time + Random.Range(1, 2);
        }
        */

        if (move)
        {
            float z = Mathf.Atan2((target.transform.position.y - transform.position.y), (target.transform.position.x - transform.position.x)) * Mathf.Rad2Deg - 90;
            transform.eulerAngles = new Vector3(0, 0, z);

            transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);

        }

        //lasketaan aika vihollisen lyönnille

        if (attackTime > 0)
        {
            attackTime -= Time.deltaTime;
        }

        if (attackTime < 0)
        {
            attackTime = 0;
        }
        if (attackTime == 0)
        {
            Attack();
            attackTime = coolDown;
        }
        

        //lasketaan arvo random dropille

        droprate = Random.Range(0, 101);

    }


    void OnCollisionExit2D(Collision2D other)
    {
        move = true;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            DestroyObject(other.gameObject);
            takeDamage();
        }
        else if (other.gameObject.tag == "Rocket")
        {
            takeDamage();
        }

    }

    
    void takeDamage()
    {
        health -= weapons.getDamage();

        if(health <= 0)
        {
            Die();          

        }
    }

    void takeExplosionDamage()
    {
        health -= 400;
        if (health <= 0)
        {
            Die();

        }
    }

    void Die()
    {
        targetplayer.setScoreText(scoreworth);
        targetplayer.Killcount += 1;
        Destroy(gameObject);
        deathsound.GetComponent<AudioSource>();
        Instantiate(deathsound, transform.position, transform.rotation);
        bloodimage.GetComponent<SpriteRenderer>();
        GameObject blood = Instantiate(bloodimage, transform.position, transform.rotation) as GameObject;
        Object.Destroy(blood, 50);


        if(droprate < 3)
        {
            hppack.GetComponent<SpriteRenderer>();
            Instantiate(hppack, transform.position, transform.rotation);
        }
        else if (droprate >10 && droprate < 13)
        {
            rifleammo.GetComponent<SpriteRenderer>();
            Instantiate(rifleammo, transform.position, transform.rotation);
        }
        else if (droprate > 20 && droprate < 23)
        {
            shotgunammo.GetComponent<SpriteRenderer>();
            Instantiate(shotgunammo, transform.position, transform.rotation);
        }
        else if (droprate > 30 && droprate < 33)
        {
            shotgunammo.GetComponent<SpriteRenderer>();
            Instantiate(rocketammo, transform.position, transform.rotation);
        }
        else if (droprate > 40 && droprate < 43)
        {
            shotgunammo.GetComponent<SpriteRenderer>();
            Instantiate(machinegunammo, transform.position, transform.rotation);
        }


    }

    void Attack()
    {

        float distance = Vector3.Distance(target.transform.position, transform.position);

        if(distance < 1.0f)

        {
            targetplayer.takeDamage(damage);
        }

    }

}
