﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;



public class HttpManager : MonoBehaviour {

    public static HttpManager instance;

    public event Action<PlayerContainer> OnPlayerReceived;
    public event Action<PlayerContainer[],int?> OnPlayersReceived;
    public event Action<double[]> OnAverageMapDataReceived;

    private string playername ="";

    public MapContainer NewMap { get; set; }
    public PlayerContainer[] GetAllPlayers { get; private set; }


	// Use this for initialization
	void Start () {

        if (instance == null)
            instance = this;
        else
            Destroy(this);

        DontDestroyOnLoad(instance);
	}

    public void setPlayerName(string name)
    {
        playername = name;
    }

    public void postPlayerData()
    {
        StartCoroutine(HttpPostPlayer());
    }

    IEnumerator HttpPostPlayer()
    {
        // Mapin infot -> Ei välttämättä näin :D.
        /*string mapBodyJson = "{'mapName' : '" + _map.MapName + "',"
            + "'score' : '" + _map.Score + "',"
            + "'killCount' : '" + _map.KillCount + "',"
            + "'favouriteweapontimer' : '" + _map.FavouriteWeaponTimer + "',"
            + "'favouriteweapon' : '" + _map.FavouriteWeapon + "',"
            + "'lifeTime' : '" + _map.LifeTime + "'}";

        Debug.Log(mapBodyJson);

        // Lähetettävä data.
        string bodyJsonString = "{'name' : " + "'" + playername + "',"
            + "'map' : " + mapBodyJson + "}";
        */

        // Mapin infot yms Newtonsoftilla.
        NewPlayerContainer newPlayer = new NewPlayerContainer(playername, NewMap);

        string json = JsonConvert.SerializeObject(newPlayer);

        var request = new UnityWebRequest("localhost:5000/api/players", "POST");
     
        // Muutos operaatio.
        byte[] bodyRaw = System.Text.Encoding.UTF8.GetBytes(json);
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        request.SetRequestHeader("Content-Type", "application/json");

        Debug.Log(json);
        yield return request.Send();

        Debug.Log("Status Code: " + request.responseCode);
     
    }

    public void getPlayerByName(string name)
    {
        StartCoroutine(HttpGetByName(name));
    }

    IEnumerator HttpGetByName(string name)
    {
        // Requesti.
        var request = new UnityWebRequest("localhost:5000/api/players/" + name, "GET");
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");

        yield return request.Send();

        Debug.Log("Status Code: " + request.responseCode);

        // Convertointi.
        string responseJson = System.Text.Encoding.UTF8.GetString(request.downloadHandler.data);

        if (OnPlayerReceived != null)
            OnPlayerReceived(JsonConvert.DeserializeObject<PlayerContainer>(responseJson));

        Debug.Log(request.downloadHandler.text);

    }

    public void GetPlayers()
    {
        StartCoroutine(HttpGetPlayers(null));
    }

    IEnumerator HttpGetPlayers(int? maporder)
    {
        // Requesti.
        var request = new UnityWebRequest("localhost:5000/api/players", "GET");
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");

        yield return request.Send();

        Debug.Log("Status Code: " + request.responseCode);

        // Convertointi.
        string responseJson = System.Text.Encoding.UTF8.GetString(request.downloadHandler.data);

        // Kutsutaan delegaattia ScoreTextEditorissa.
        if (OnPlayersReceived != null)
            OnPlayersReceived(JsonConvert.DeserializeObject<PlayerContainer[]>(responseJson), null);

        Debug.Log(request.downloadHandler.text);
    }

    public void GetTopScoreByMap(int maporder, string search)
    {
        StartCoroutine(HttpGetTopScoreByMap(maporder, search));
    }

    IEnumerator HttpGetTopScoreByMap(int? maporder, string search)
    {
        // Request top 10 pelaajaa tietystä polusta.
        var request = new UnityWebRequest("localhost:5000/api/players/top10?mapOrder=" + maporder + "&score=true", "GET");

        // Eri parametrien hakupolut
        if(search == "killcount")
            request = new UnityWebRequest("localhost:5000/api/players/top10?mapOrder=" + maporder + "&killCount=true", "GET");
        else if(search == "lifetime")
            request = new UnityWebRequest("localhost:5000/api/players/top10?mapOrder=" + maporder + "&lifeTime=true", "GET");
        else if(search == "score")
            request = new UnityWebRequest("localhost:5000/api/players/top10?mapOrder=" + maporder + "&score=true", "GET");

        // kyselyn lähetys
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");
        yield return request.Send();

        Debug.Log("Status Code: " + request.responseCode);

        // Convertointi.
        string responseJson = System.Text.Encoding.UTF8.GetString(request.downloadHandler.data);

        // Kutsutaan delegaattia ScoreTextEditorissa.
        if (OnPlayersReceived != null)
            OnPlayersReceived(JsonConvert.DeserializeObject<PlayerContainer[]>(responseJson),maporder);
    }

    public void getAverageMapData()
    {
        StartCoroutine(HttpAverageMapData());
    }

    IEnumerator HttpAverageMapData()
    {
        var request = new UnityWebRequest("localhost:5000/api/players/average", "GET");

        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");
        yield return request.Send();

        string responseJson = System.Text.Encoding.UTF8.GetString(request.downloadHandler.data);

        // Kutsutaan delegaattia ScoreTextEditorissa.
        if (OnAverageMapDataReceived!= null)
            OnAverageMapDataReceived(JsonConvert.DeserializeObject<double[]>(responseJson));


    }

}
