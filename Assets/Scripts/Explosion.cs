﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour {

    Animator anim;
    public AudioSource explosion;

    // Use this for initialization
    void Start () {

        anim = GetComponent<Animator>();
        ExplosionDamage(transform.position, 1.3f);
        explosion.Play();
        anim.SetTrigger("explosion");

    }
	
	// Update is called once per frame
	void Update () {
       
	}

    void DestroyMe()
    {
        Destroy(gameObject);
    }

    void ExplosionDamage(Vector2 center, float radius)
    {
        Collider2D[] hitColliders = Physics2D.OverlapCircleAll(center,radius);
        int i = 0;
        while (i< hitColliders.Length)
        {
            hitColliders[i].SendMessage("takeExplosionDamage",SendMessageOptions.DontRequireReceiver);
            i++;
        }
    }
}
