﻿using UnityEngine;
using System.Collections;

public class Crosshair : MonoBehaviour {

    public Texture2D crosshairImage;
    public float crosshairScale = 1;

    // Use this for initialization
    void Start () {

        Cursor.visible = false;
	
	}

    void Update () {
	
	}

    void OnGUI()
    {
                float xMin = Screen.width - (Screen.width - Input.mousePosition.x) - (crosshairImage.width / 2);
                float yMin = (Screen.height - Input.mousePosition.y) - (crosshairImage.height / 2);
                GUI.DrawTexture(new Rect(xMin, yMin, crosshairImage.width, crosshairImage.height), crosshairImage);        
    }
}
