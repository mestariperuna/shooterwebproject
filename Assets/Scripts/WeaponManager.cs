﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class WeaponManager : MonoBehaviour {

    protected int rifleAmmo = 300;
    protected int shotgunAmmo = 27;
    protected int sniperAmmo = 0;
    protected int machinegunAmmo = 0;
    public float firerate = 0;
    public float reloadrate = 0;
    public Transform ammoSpawn;
    public Transform[] shotgunSpawn;

    private float recoil = 0.0f;


    public Rigidbody2D ammoPrefab;
    public Rigidbody2D sniperPrefab;
    public Rigidbody2D shotgunPrefab;
    private Rigidbody2D currentPrefab;
    public Rigidbody2D rocketPrefab;

    public AudioSource click;
    public AudioSource launch;
    public AudioSource singleshot;
    public AudioSource weapload;
    public AudioSource shotgun;
    private AudioSource currentAudio;

    public Text ammotext;

    Animator anim;


    public Sprite pistolsprite;
    public Sprite riflesprite;

    Weapon currentweapon = new Weapon();
    Rifle myrifle = new Rifle();
    RocketLauncher myrl = new RocketLauncher();
    Shotgun myshotgun = new Shotgun();
    Machinegun mymachinegun = new Machinegun();

    List<Weapon> weaponlist = new List<Weapon>();


    // Use this for initialization
    void Start ()
    {
        anim = GetComponent<Animator>();

        currentweapon = myrifle;
        currentPrefab = ammoPrefab;
        currentAudio = singleshot;

        setAmmoText();

        weaponlist.Add(myrifle);
        weaponlist.Add(myrl);
        weaponlist.Add(mymachinegun);
        weaponlist.Add(myshotgun);
}

    // Update is called once per frame
    void Update ()
    {

        //Aseiden valinta numeronäppäimillä

        if (Input.GetKeyDown("1"))
        {
            currentweapon = myrifle;
            currentPrefab = ammoPrefab;
            currentAudio = singleshot;
            setAmmoText();
        }
        else if(Input.GetKeyDown("2"))
        {
            currentweapon = myshotgun;
            currentPrefab = shotgunPrefab;
            currentAudio = shotgun;
            setAmmoText();
        }
        else if(Input.GetKeyDown("3"))
        {
            currentweapon = myrl;
            currentPrefab = rocketPrefab;
            currentAudio = launch;
            setAmmoText();
        }
        else if(Input.GetKeyDown("4"))
        {
            currentweapon = mymachinegun;
            currentPrefab = sniperPrefab;
            currentAudio = singleshot;
            setAmmoText();
        }

        // Tulitus, tarkistetaan onko firemode semiauto vai fullauto, ja sen mukaan määritellään hiiren tarkastelu

        if(currentweapon.getFireMode())
        {
            if(Input.GetMouseButton(0))
            {
                Fire();
            }
        }
        else if(!currentweapon.getFireMode())
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (currentweapon == myshotgun)
                {
                    FireShotgun();
                }
                else
                {
                    Fire();
                }
            }
        }
        

        //Lataus

        if(Input.GetKeyDown("r"))
        {        
            if(currentweapon.getCurrentAmmo() < currentweapon.getClipSize())
            {
                Invoke("ReloadWeapon", currentweapon.getReloadSpeed());
               // weapload.Play();
            }           
        }

        // Mongodb juttuja. Timer aseille.
        currentweapon.timer += Time.deltaTime;
    }

    //getterit

    public Rigidbody2D getPrefab()
    {
        return currentPrefab;
    } 

    public Transform getAmmoSpawn()
    {
        return ammoSpawn;
    }

    public Weapon getWeapon()
    {
        return currentweapon;
    }

    public Weapon getFavouriteWeapon()
    {
        Weapon tempweapon = myrifle;

        for (int i = 0; i < weaponlist.Count; i++)
        { 
            if (weaponlist[i].timer > tempweapon.timer)
                tempweapon = weaponlist[i];
        }

        return tempweapon;
    }


    // uutta paskaa

    public void ReloadWeapon()
    {
        currentweapon.Reload();
        weapload.Play();
        setAmmoText();
    }


    public void Fire()
    {
        if (Time.time > firerate)
        {
            if (currentweapon.getCurrentAmmo() > 0)
            {
                firerate = Time.time + currentweapon.getFireRate();
                anim.SetTrigger("Firing_");
                Instantiate(currentPrefab, ammoSpawn.transform.position, transform.rotation);
                currentAudio.Play();
                currentweapon.setAmmo(-1);
                setAmmoText();
                recoil += 0.1f;
            }
            else
            {
                firerate = Time.time + currentweapon.getFireRate();
                click.Play();
            }

        }

    }

    public void FireShotgun()
    {
        if(Time.time > firerate)
        {
            firerate = Time.time + currentweapon.getFireRate();
            if (currentweapon.getCurrentAmmo() > 0 )
            {
                anim.SetTrigger("Firing_");

                for (int i = 1; i < 8; i++)
                {
                    Instantiate(currentPrefab, shotgunSpawn[i].transform.position, shotgunSpawn[i].transform.rotation);
                }

                currentAudio.Play();
                currentweapon.setAmmo(-1);
                setAmmoText();
               }
        }
        else
        {
            click.Play();
        }


    }

    public float getRecoil()
    {
        return recoil;
    }

    public void setAmmoText()
    {
        ammotext.text = currentweapon.getName() + " Ammo: " + currentweapon.getCurrentAmmo() + "/" + currentweapon.getAmmoCarried();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Rifleammo")
        {
            DestroyObject(other.gameObject);
            myrifle.setAmmoCarried(90);
            setAmmoText();
        }
        else if(other.gameObject.tag == "Shotgunammo")
        {
            DestroyObject(other.gameObject);
            myshotgun.setAmmoCarried(24);
            setAmmoText();
        }
        else if(other.gameObject.tag == "Rocketammo")
        {
            DestroyObject(other.gameObject);
            myrl.setAmmoCarried(10);
            setAmmoText();
        }
        else if(other.gameObject.tag == "Machinegunammo")
        {
            DestroyObject(other.gameObject);
            mymachinegun.setAmmoCarried(90);
            setAmmoText();
        }
    }

    public int getDamage()
    {
        return currentweapon.getDamage();
    }

}
