﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {

    public Rigidbody2D bugGPrefab;
    public Rigidbody2D bugRPrefab;
    public Rigidbody2D bugLargePrefab;
    public Transform[] spawnPoints;
    private float minSpawnTime = 0.7f;
    private float maxSpawnTime = 7f;
    private float timer;
 


	// Use this for initialization
	void Start ()
    {
        Spawn();    
    }
	
	// Update is called once per frame
	void Update ()
    {
        //timeri kuluneelle ajalle
        timer += Time.deltaTime;
    }

    void Spawn()
    {
        
        //kasvatetaan vihollisten spawnaus-ratea ajan kuluessa

        if (timer >= 15f)
        {
            maxSpawnTime = 5.0f;
        }
        if (timer >= 30f)
        {
            maxSpawnTime = 4.0f;
        }
        if (timer >= 60f)
        {
            maxSpawnTime = 3.0f;
        }
        if (timer >= 120f)
        {
            maxSpawnTime = 2.0f;
        }
        if (timer >= 180f)
        {
            maxSpawnTime = 1.5f;
        }

        //spawnataan vihuja random paikkoihin

        int spawnPointIndex = Random.Range(0, spawnPoints.Length);
        int enemyIndex = Random.Range(0, 20);


        //spawnataan random vihu

        if(enemyIndex >= 5 && enemyIndex < 19)
        {
            Instantiate(bugGPrefab, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
        }else if (enemyIndex <5)
        {
            Instantiate(bugRPrefab, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
        }else if (enemyIndex == 19)
        {
            Instantiate(bugLargePrefab, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
        }


        //kutsutaan spawn-funktiota random ajan välein

        Invoke("Spawn", Random.Range(minSpawnTime, maxSpawnTime));

    }
}
