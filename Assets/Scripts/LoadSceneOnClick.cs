﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadSceneOnClick : MonoBehaviour {

    public GameObject secondCanvas;

    public void LoadByIndex(int sceneIndex)
    {
        SceneManager.LoadScene(sceneIndex);

    }

    public void ActivateCanvas()
    {
        secondCanvas.SetActive(true);
    }

    public void DisableCanvas()
    {
        secondCanvas.SetActive(false);
    }

    public void Quit()
    {
        Application.Quit();
    }

}
