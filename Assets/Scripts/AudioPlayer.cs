﻿using UnityEngine;
using System.Collections;

public class AudioPlayer : MonoBehaviour
{

    //public AudioSource[] playlist;
    AudioSource currentclip;
    public AudioClip[] _playlist;
    int i;

    void Start()
    {
        currentclip = GetComponent<AudioSource>();
        i = Random.Range(0, _playlist.Length);
        currentclip.clip = _playlist[i];
        currentclip.Play();
    }

    void Update()
    {
        if(!currentclip.isPlaying)
        {
            i = Random.Range(0, _playlist.Length);
            currentclip.clip = _playlist[i];
            currentclip.Play();
        }
    }

}