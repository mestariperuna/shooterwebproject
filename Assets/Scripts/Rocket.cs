﻿using UnityEngine;
using System.Collections;

public class Rocket : MonoBehaviour
{

    private Rigidbody2D ammo;
    private GameObject player;
    public int ammoSpeed;
    public GameObject bloodPrefab;
    public GameObject explosion;

    private WeaponManager weapons;

    //Animator anim;

    // Use this for initialization
    void Start()
    {

        player = GameObject.FindGameObjectWithTag("Player");
        weapons = player.GetComponent<WeaponManager>();


        ammo = GetComponent<Rigidbody2D>();
        Vector3 mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.rotation = Quaternion.LookRotation(Vector3.forward, mouse - transform.position);
        ammo.AddForce(transform.up * weapons.getWeapon().getAmmoSpeed(), ForceMode2D.Impulse);

      //  anim = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Enemy")
        {
            Instantiate(bloodPrefab, transform.position, transform.rotation);
            Instantiate(explosion, transform.position, transform.rotation);
            Destroy(gameObject);

        }
    }

    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

    void takeExplosionDamage()
    {
        Debug.Log("iha testi vaa :D");
    }

 
}
