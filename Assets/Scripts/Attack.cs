﻿using UnityEngine;
using System.Collections;

public class Attack : MonoBehaviour {

    public GameObject target;
    public float attackTime;
    public float coolDown = 2;

	// Use this for initialization
	void Start () {

        attackTime = 0;
	
	}
	
	// Update is called once per frame
	void Update () {

        if(attackTime > 0)
        {
            attackTime -= Time.deltaTime;
        }

        if(attackTime < 0)
        {
            attackTime = 0;
        }

        if(attackTime == 0)
        {
            Hit();
            attackTime = coolDown;
        }
	
	}

    private void Hit()
    {

    }
}
