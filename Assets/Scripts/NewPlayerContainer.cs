﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class NewPlayerContainer{

    public string Name { get; set; }
    public MapContainer Map { get; set; }

    public NewPlayerContainer(string name, MapContainer map)
    {
        Name = name;
        Map = map;
    }

}
