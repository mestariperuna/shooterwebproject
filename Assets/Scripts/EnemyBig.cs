﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnemyBig : MonoBehaviour
{


    private Transform target;
    public float speed = 10;
    public float health = 100;
    private bool move = true;
    private GameObject player;
    private PlayerController targetplayer;
    private WeaponManager weapons;
    public int scoreworth;
    public int damage;

    public float attackTime;
    public float coolDown;

    public GameObject bloodimage;
    public GameObject deathsound;
    public Rigidbody2D smallenemy;

    public GameObject spawnerprefab;
    private GameObject droppedweapon;
    public GameObject hppack;
    public GameObject rifleammo;
    public GameObject shotgunammo;
    public GameObject rocketammo;
    public GameObject machinegunammo;

    private int droprate;

    private float minSpawnTime = 5;
    private float maxSpawnTime = 10;



    // Use this for initialization
    void Start()
    {
        
        player = GameObject.FindGameObjectWithTag("Player");
        target = player.transform;
        targetplayer = player.GetComponent<PlayerController>();

        weapons = player.GetComponent<WeaponManager>();

        attackTime = 0;
        coolDown = 2.0f;

        Spawn();


    }

    // Update is called once per frame
    void Update()
    {


        if (move)
        {
            float z = Mathf.Atan2((target.transform.position.y - transform.position.y), (target.transform.position.x - transform.position.x)) * Mathf.Rad2Deg - 90;
            transform.eulerAngles = new Vector3(0, 0, z);

            transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);

        }

        //lasketaan aika vihollisen lyönnille

        if (attackTime > 0)
        {
            attackTime -= Time.deltaTime;
        }

        if (attackTime < 0)
        {
            attackTime = 0;
        }
        if (attackTime == 0)
        {
            Attack();
            attackTime = coolDown;
        }

        //randomoidaan droprate
        droprate = Random.Range(0, 101);

    }


    void OnCollisionExit2D(Collision2D other)
    {
        move = true;
    }


    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            DestroyObject(other.gameObject);
            takeDamage();
        }
        else if (other.gameObject.tag == "Rocket")
        {
            takeDamage();
        }

        if (other.gameObject.tag == "Explosion")
        {
            health -= 400;
            Debug.Log("pamahti :D:D");
            if (health <= 0)
            {
                Die();

            }
        }
    }

    void takeDamage()
    {
        health -= weapons.getDamage();

        if (health <= 0)
        {
            Die();

        }
    }

    void takeExplosionDamage()
    {
        health -= 400;
        if (health <= 0)
        {
            Die();

        }
    }

    void Die()
    {
        targetplayer.Killcount += 1;
        targetplayer.setScoreText(scoreworth);
        Destroy(gameObject);
        deathsound.GetComponent<AudioSource>();
        Instantiate(deathsound, transform.position, transform.rotation);
        bloodimage.GetComponent<SpriteRenderer>();
        GameObject blood = Instantiate(bloodimage, transform.position, transform.rotation) as GameObject;
        Object.Destroy(blood, 50);

        for (int i = 0; i < 5; i++)
        {
            Vector3 temp = new Vector3(0.2f, 0, 0);
            Instantiate(smallenemy, transform.position += temp, transform.rotation);
        }


        if (droprate < 7)
        {
            hppack.GetComponent<SpriteRenderer>();
            Instantiate(hppack, transform.position, transform.rotation);
        }
        else if (droprate > 7 && droprate < 15)
        {
            rifleammo.GetComponent<SpriteRenderer>();
            Instantiate(rifleammo, transform.position, transform.rotation);
        }
        else if (droprate > 20 && droprate < 28)
        {
            shotgunammo.GetComponent<SpriteRenderer>();
            Instantiate(shotgunammo, transform.position, transform.rotation);
        }
        else if (droprate > 30 && droprate < 35)
        {
            shotgunammo.GetComponent<SpriteRenderer>();
            Instantiate(rocketammo, transform.position, transform.rotation);
        }
        else if (droprate > 40 && droprate < 47)
        {
            shotgunammo.GetComponent<SpriteRenderer>();
            Instantiate(machinegunammo, transform.position, transform.rotation);
        }


    }

    void Spawn()
    {
        Invoke("Spawn", Random.Range(minSpawnTime, maxSpawnTime));
        Vector3 temp = new Vector3(0.2f, 0, 0);
        Instantiate(smallenemy, transform.position += temp, transform.rotation);
    }

    void Attack()
    {

        float distance = Vector3.Distance(target.transform.position, transform.position);

        if (distance < 1.0f)

        {
            targetplayer.takeDamage(damage);
            Debug.Log("lyönti");
        }

    }

}
