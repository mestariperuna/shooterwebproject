﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class PlayerContainer{
    public string Name { get; set; }
    public List<MapContainer> Maps { get; set; }

    public PlayerContainer(string namepar, List<MapContainer> mappar)
    {
        Name = namepar;
        Maps = mappar;
    }
}
