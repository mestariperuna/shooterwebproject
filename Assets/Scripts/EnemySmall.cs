﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnemySmall : MonoBehaviour
{


    private Transform target;
    public float speed = 10;
    public float health = 100;
    private bool move = true;
    private GameObject player;
    private PlayerController targetplayer;
    private WeaponManager weapons;
    public int scoreworth;
    public int damage;


    public float attackTime;
    public float coolDown;

    public GameObject bloodimage;
    public GameObject deathsound;


    // Use this for initialization
    void Start()
    {

        player = GameObject.FindGameObjectWithTag("Player");
        target = player.transform;
        targetplayer = player.GetComponent<PlayerController>();

        weapons = player.GetComponent<WeaponManager>();

        attackTime = 0;
        coolDown = 2.0f;


    }

    // Update is called once per frame
    void Update()
    {


        if (move)
        {
            float z = Mathf.Atan2((target.transform.position.y - transform.position.y), (target.transform.position.x - transform.position.x)) * Mathf.Rad2Deg - 90;
            transform.eulerAngles = new Vector3(0, 0, z);

            transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);

        }

        //lasketaan aika vihollisen lyönnille

        if (attackTime > 0)
        {
            attackTime -= Time.deltaTime;
        }

        if (attackTime < 0)
        {
            attackTime = 0;
        }
        if (attackTime == 0)
        {
            Attack();
            attackTime = coolDown;
        }

    }


    void OnCollisionExit2D(Collision2D other)
    {
        move = true;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            DestroyObject(other.gameObject);
            takeDamage();
        }
        else if (other.gameObject.tag == "Rocket")
        {
            takeDamage();
        }

    }


    void takeDamage()
    {
        health -= weapons.getDamage();

        if (health <= 0)
        {
            Die();

        }
    }

    void takeExplosionDamage()
    {
        health -= 400;
        if (health <= 0)
        {
            Die();

        }
    }

    void Die()
    {
        targetplayer.Killcount += 1;
        targetplayer.setScoreText(scoreworth);
        Destroy(gameObject);
        deathsound.GetComponent<AudioSource>();
        Instantiate(deathsound, transform.position, transform.rotation);
        bloodimage.GetComponent<SpriteRenderer>();
        GameObject blood = Instantiate(bloodimage, transform.position, transform.rotation) as GameObject;
        Object.Destroy(blood, 50);

    }

    void Attack()
    {

        float distance = Vector3.Distance(target.transform.position, transform.position);

        if (distance < 1.0f)

        {
            targetplayer.takeDamage(damage);
        }

    }

}
