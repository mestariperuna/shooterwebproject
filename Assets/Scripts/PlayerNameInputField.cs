﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerNameInputField : MonoBehaviour {


    static string playerNamePrefKey = "PlayerName";
    private PlayerController player;
    InputField _inputField;

    // Use this for initialization
    void Start () {
        player = GameObject.FindObjectOfType<PlayerController>();

        string defaultName = "";
        _inputField = this.GetComponent<InputField>();
        if (_inputField != null)
        {
            if (PlayerPrefs.HasKey(playerNamePrefKey))
            {
                defaultName = PlayerPrefs.GetString(playerNamePrefKey);
                _inputField.text = defaultName;
            }
        }
    }
	
    public void SetPlayerName()
    {
        string name = _inputField.text;
        PlayerPrefs.SetString(playerNamePrefKey, name);
    }

    public void SetPlayerRef()
    {
        player.Name = PlayerPrefs.GetString(playerNamePrefKey);
        Debug.Log("HTTP-Manageriin lähetetty nimi: " + player.Name);
        HttpManager.instance.setPlayerName(PlayerPrefs.GetString(playerNamePrefKey));
    }

}
