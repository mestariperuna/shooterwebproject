﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
using System.IO;
using System.Text;

public class PlayerController : MonoBehaviour
{

    public Rigidbody2D ammoPrefab;
    public float movementSpeed;
    public float rotateSpeed;
    private Vector2 movement;
    public float health = 100;

    public GameObject deadcanvas;
    public GameObject crosshair;

    public Text healthText;
    public Text scoreText;
    public Text deadScore;
    public int score;

    // Mongodb projektijuttuja -17.
    public int Killcount { get; set; }
    public float Lifetime { get; set; }
    public string Name { get; set; }
    private string mapName;
    private WeaponManager weapons;

    ScoreWriter sw = new ScoreWriter();

    void Start()
    {
        Cursor.visible = false;
        crosshair.SetActive(true);
        deadcanvas.SetActive(false);
        score = 0;
        setScoreText(0);
        setHealthText();

        // Mongodb juttuja.
        weapons = GetComponent<WeaponManager>();
        Lifetime = 0;
        mapName = SceneManager.GetActiveScene().name;
        string defaultName = "";
    }

    void Update()
    {
        // Mongodb lifetimeri.
        Lifetime += Time.deltaTime;
    }

    void FixedUpdate()
    {       
        Vector3 mousepos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.rotation = Quaternion.LookRotation(Vector3.forward, mousepos - transform.position);

        movement = getInput();
        transform.Translate(movementSpeed * movement.x * Time.deltaTime, movementSpeed * movement.y * Time.deltaTime, 0f, Space.World);

    }

    //haetaan inputti 
    public Vector2 getInput()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        Vector2 input = new Vector2(h, v);

        if(input.sqrMagnitude > 1)
        {
            input.Normalize();
            return input;
        }
        else
        {
            return input;
        }
    }

    public void takeDamage(int d)
    {
        setHealthText();
        health -= d;
        if (health <= 0)
        {
            Die();
        }

        if(health < 0)
        {
            health = 0;
        }
    }


    //collision entterit poweruppeihin ja aseisiin yms.

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Healthpack")
        {
            DestroyObject(other.gameObject);
            Debug.Log("hppp");
            health += 50;
            if(health > 100)
            {
                health = 100;
            }
            setHealthText();
        }
    }

    public void setScoreText(int s)
  {
        score += s;
        scoreText.text = "Score: " + score.ToString();
  }

    void setHealthText()
    {
        healthText.text = "Health: " + health.ToString();
    }

    void setDeadScore()
    {
        deadScore.text = "Score: " + score; 
    }

    void Die()
    {
        Debug.Log("kuolit, harmi");
        Time.timeScale = 0;
        deadcanvas.SetActive(true);
        setDeadScore();
        Cursor.visible = true;
        crosshair.SetActive(false);
        

        // Lifetime countteri ja formatointi (siirtyy scoreboardille).
        float minutes = Mathf.Floor(Lifetime / 60);
        float seconds = Lifetime % 60;
        float fraction = (Lifetime * 100) % 100;
        string lifetimestring = string.Format("{0:00}.{1:00}.{2:0}", minutes, seconds, fraction);

        // Tallennetaan data containeriin.
        Weapon favouriteweapon = weapons.getFavouriteWeapon();
        HttpManager.instance.NewMap = new MapContainer(mapName, score, Killcount, favouriteweapon.getName(), favouriteweapon.timer, Lifetime);
        
        

        // Weapon timerin katsominen
       /* Debug.Log(favouriteweapon.getName() + favouriteweapon.timer);
        Debug.Log("Kills: " + Killcount);
        Debug.Log(mapName);
        Debug.Log(lifetimestring);*/
    }

}